﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decypher
{
    public class A1_Codebook
    {
        public Dictionary<string, int> recnik;

        public A1_Codebook()
        {
            recnik = new Dictionary<string, int>();
        }

        private void SortRecnik()
        {
            var items = from pair in recnik
                        orderby pair.Key.Length descending
                        select pair;

            recnik = items.ToDictionary(x => x.Key, x => x.Value);
        }

        public void SaveToFile()
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            var fi = new System.IO.FileInfo(@"D:\Fakultet\4. godina\7. semestar\Zastita informacija\Projekat\ZI - 15387\codebook.bin");

            using (var binaryFile = fi.Create())
            {
                binaryFormatter.Serialize(binaryFile, recnik);
                binaryFile.Flush();
            }
        }

        public void ReadFromFile()
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            var fi = new System.IO.FileInfo(@"D:\Fakultet\4. godina\7. semestar\Zastita informacija\Projekat\ZI - 15387\codebook.bin");

            using (var binaryFile = fi.OpenRead())
            {
                recnik = (Dictionary<string, int>)binaryFormatter.Deserialize(binaryFile);
            }

            SortRecnik();

        }

        public string Dekriptuj(string recenica)
        {
            string[] rec = recenica.Split(' ');
            List<string> keys1 = this.recnik.Keys.ToList();
            List<int> values1 = this.recnik.Values.ToList();

            for (int i = 0; i < rec.Length; i++)
            {
                if (rec[i] == "")
                    continue;

                try
                {
                    if (values1.Contains(Int32.Parse(rec[i])))
                    {
                        int j = values1.IndexOf(Int32.Parse(rec[i]));
                        rec[i] = keys1[j].ToString();
                    }
                }
                catch (Exception e)
                {
                    List<string> keys11 = new List<string>(this.recnik.Keys);
                    List<int> values11 = new List<int>(this.recnik.Values);

                    var list11 = keys11.Zip(values11, (k, v) => new { Key = k, Value = v });

                    foreach (var l in list11)
                    {
                        rec[i] = rec[i].Replace(l.Value.ToString(), l.Key);
                    }
                }
                
            }

            recenica = String.Join<string>(" ", rec.ToArray());
            

            return recenica;
        }
    }
}
