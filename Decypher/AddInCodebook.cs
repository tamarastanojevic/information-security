﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decypher
{
    public partial class AddInCodebook : Form
    {
        public TextBox t1;
        public TextBox t2;

        public AddInCodebook()
        {
            InitializeComponent();

            t1 = tbWord;
            t2 = tbCode;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
