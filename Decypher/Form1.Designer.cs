﻿namespace Decypher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbInput2 = new System.Windows.Forms.TextBox();
            this.btnSaveCodebook = new System.Windows.Forms.Button();
            this.dgvCodebook2 = new System.Windows.Forms.DataGridView();
            this.btnDekriptuj = new System.Windows.Forms.Button();
            this.tbOutput2 = new System.Windows.Forms.TextBox();
            this.pCodebook = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pRC6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRC6 = new System.Windows.Forms.Button();
            this.tbRC6Output = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbRC6Key = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbRC6Input = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuAlgorithm = new System.Windows.Forms.ToolStripMenuItem();
            this.codebookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rC6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEAPCBCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pTEA = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbIV = new System.Windows.Forms.TextBox();
            this.btnTEADecrypt = new System.Windows.Forms.Button();
            this.tbTEAOutput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTEAKey = new System.Windows.Forms.TextBox();
            this.tbTEAInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pStart = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodebook2)).BeginInit();
            this.pCodebook.SuspendLayout();
            this.pRC6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.pTEA.SuspendLayout();
            this.pStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(53, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "CODEBOOK";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(345, 370);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 25);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbInput2
            // 
            this.tbInput2.Location = new System.Drawing.Point(13, 49);
            this.tbInput2.Multiline = true;
            this.tbInput2.Name = "tbInput2";
            this.tbInput2.Size = new System.Drawing.Size(216, 100);
            this.tbInput2.TabIndex = 0;
            // 
            // btnSaveCodebook
            // 
            this.btnSaveCodebook.Location = new System.Drawing.Point(235, 370);
            this.btnSaveCodebook.Name = "btnSaveCodebook";
            this.btnSaveCodebook.Size = new System.Drawing.Size(101, 25);
            this.btnSaveCodebook.TabIndex = 5;
            this.btnSaveCodebook.Text = "save codebook";
            this.btnSaveCodebook.UseVisualStyleBackColor = true;
            this.btnSaveCodebook.Click += new System.EventHandler(this.btnSaveCodebook_Click);
            // 
            // dgvCodebook2
            // 
            this.dgvCodebook2.AllowUserToDeleteRows = false;
            this.dgvCodebook2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCodebook2.Location = new System.Drawing.Point(235, 7);
            this.dgvCodebook2.Name = "dgvCodebook2";
            this.dgvCodebook2.Size = new System.Drawing.Size(185, 357);
            this.dgvCodebook2.TabIndex = 4;
            // 
            // btnDekriptuj
            // 
            this.btnDekriptuj.Location = new System.Drawing.Point(72, 170);
            this.btnDekriptuj.Name = "btnDekriptuj";
            this.btnDekriptuj.Size = new System.Drawing.Size(108, 56);
            this.btnDekriptuj.TabIndex = 3;
            this.btnDekriptuj.Text = "decrypt";
            this.btnDekriptuj.UseVisualStyleBackColor = true;
            this.btnDekriptuj.Click += new System.EventHandler(this.btnDekriptuj_Click);
            // 
            // tbOutput2
            // 
            this.tbOutput2.Location = new System.Drawing.Point(13, 247);
            this.tbOutput2.Multiline = true;
            this.tbOutput2.Name = "tbOutput2";
            this.tbOutput2.Size = new System.Drawing.Size(216, 100);
            this.tbOutput2.TabIndex = 1;
            // 
            // pCodebook
            // 
            this.pCodebook.Controls.Add(this.label11);
            this.pCodebook.Controls.Add(this.label10);
            this.pCodebook.Controls.Add(this.label1);
            this.pCodebook.Controls.Add(this.btnAdd);
            this.pCodebook.Controls.Add(this.tbInput2);
            this.pCodebook.Controls.Add(this.btnSaveCodebook);
            this.pCodebook.Controls.Add(this.dgvCodebook2);
            this.pCodebook.Controls.Add(this.btnDekriptuj);
            this.pCodebook.Controls.Add(this.tbOutput2);
            this.pCodebook.Location = new System.Drawing.Point(12, 27);
            this.pCodebook.Name = "pCodebook";
            this.pCodebook.Size = new System.Drawing.Size(426, 406);
            this.pCodebook.TabIndex = 10;
            this.pCodebook.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 231);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Output text:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Input text:";
            // 
            // pRC6
            // 
            this.pRC6.Controls.Add(this.label9);
            this.pRC6.Controls.Add(this.label8);
            this.pRC6.Controls.Add(this.btnRC6);
            this.pRC6.Controls.Add(this.tbRC6Output);
            this.pRC6.Controls.Add(this.label3);
            this.pRC6.Controls.Add(this.tbRC6Key);
            this.pRC6.Controls.Add(this.label2);
            this.pRC6.Controls.Add(this.tbRC6Input);
            this.pRC6.Location = new System.Drawing.Point(12, 27);
            this.pRC6.Name = "pRC6";
            this.pRC6.Size = new System.Drawing.Size(418, 382);
            this.pRC6.TabIndex = 12;
            this.pRC6.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 254);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Output text:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Input text:";
            // 
            // btnRC6
            // 
            this.btnRC6.Location = new System.Drawing.Point(166, 196);
            this.btnRC6.Name = "btnRC6";
            this.btnRC6.Size = new System.Drawing.Size(75, 53);
            this.btnRC6.TabIndex = 16;
            this.btnRC6.Text = "decrypt";
            this.btnRC6.UseVisualStyleBackColor = true;
            this.btnRC6.Click += new System.EventHandler(this.btnRC6_Click);
            // 
            // tbRC6Output
            // 
            this.tbRC6Output.Location = new System.Drawing.Point(14, 270);
            this.tbRC6Output.Multiline = true;
            this.tbRC6Output.Name = "tbRC6Output";
            this.tbRC6Output.Size = new System.Drawing.Size(388, 99);
            this.tbRC6Output.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Key";
            // 
            // tbRC6Key
            // 
            this.tbRC6Key.Location = new System.Drawing.Point(53, 137);
            this.tbRC6Key.Multiline = true;
            this.tbRC6Key.Name = "tbRC6Key";
            this.tbRC6Key.Size = new System.Drawing.Size(349, 41);
            this.tbRC6Key.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(175, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "RC6";
            // 
            // tbRC6Input
            // 
            this.tbRC6Input.Location = new System.Drawing.Point(14, 42);
            this.tbRC6Input.Multiline = true;
            this.tbRC6Input.Name = "tbRC6Input";
            this.tbRC6Input.Size = new System.Drawing.Size(388, 80);
            this.tbRC6Input.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAlgorithm});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(449, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuAlgorithm
            // 
            this.menuAlgorithm.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codebookToolStripMenuItem,
            this.rC6ToolStripMenuItem,
            this.tEAPCBCToolStripMenuItem});
            this.menuAlgorithm.Name = "menuAlgorithm";
            this.menuAlgorithm.Size = new System.Drawing.Size(77, 20);
            this.menuAlgorithm.Text = "Algortithm";
            // 
            // codebookToolStripMenuItem
            // 
            this.codebookToolStripMenuItem.Name = "codebookToolStripMenuItem";
            this.codebookToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.codebookToolStripMenuItem.Text = "Codebook";
            this.codebookToolStripMenuItem.Click += new System.EventHandler(this.codebookToolStripMenuItem_Click);
            // 
            // rC6ToolStripMenuItem
            // 
            this.rC6ToolStripMenuItem.Name = "rC6ToolStripMenuItem";
            this.rC6ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.rC6ToolStripMenuItem.Text = "RC6";
            this.rC6ToolStripMenuItem.Click += new System.EventHandler(this.rC6ToolStripMenuItem_Click);
            // 
            // tEAPCBCToolStripMenuItem
            // 
            this.tEAPCBCToolStripMenuItem.Name = "tEAPCBCToolStripMenuItem";
            this.tEAPCBCToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tEAPCBCToolStripMenuItem.Text = "TEA/PCBC";
            this.tEAPCBCToolStripMenuItem.Click += new System.EventHandler(this.tEAPCBCToolStripMenuItem_Click);
            // 
            // pTEA
            // 
            this.pTEA.Controls.Add(this.label7);
            this.pTEA.Controls.Add(this.label17);
            this.pTEA.Controls.Add(this.label6);
            this.pTEA.Controls.Add(this.tbIV);
            this.pTEA.Controls.Add(this.btnTEADecrypt);
            this.pTEA.Controls.Add(this.tbTEAOutput);
            this.pTEA.Controls.Add(this.label5);
            this.pTEA.Controls.Add(this.tbTEAKey);
            this.pTEA.Controls.Add(this.tbTEAInput);
            this.pTEA.Controls.Add(this.label4);
            this.pTEA.Location = new System.Drawing.Point(13, 27);
            this.pTEA.Name = "pTEA";
            this.pTEA.Size = new System.Drawing.Size(420, 394);
            this.pTEA.TabIndex = 13;
            this.pTEA.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Output text:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Input text:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "IV";
            // 
            // tbIV
            // 
            this.tbIV.Location = new System.Drawing.Point(53, 196);
            this.tbIV.Multiline = true;
            this.tbIV.Name = "tbIV";
            this.tbIV.Size = new System.Drawing.Size(268, 45);
            this.tbIV.TabIndex = 15;
            // 
            // btnTEADecrypt
            // 
            this.btnTEADecrypt.Location = new System.Drawing.Point(327, 193);
            this.btnTEADecrypt.Name = "btnTEADecrypt";
            this.btnTEADecrypt.Size = new System.Drawing.Size(75, 66);
            this.btnTEADecrypt.TabIndex = 14;
            this.btnTEADecrypt.Text = "decrypt";
            this.btnTEADecrypt.UseVisualStyleBackColor = true;
            this.btnTEADecrypt.Click += new System.EventHandler(this.btnTEADecrypt_Click);
            // 
            // tbTEAOutput
            // 
            this.tbTEAOutput.Location = new System.Drawing.Point(14, 265);
            this.tbTEAOutput.Multiline = true;
            this.tbTEAOutput.Name = "tbTEAOutput";
            this.tbTEAOutput.Size = new System.Drawing.Size(388, 122);
            this.tbTEAOutput.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Key";
            // 
            // tbTEAKey
            // 
            this.tbTEAKey.Location = new System.Drawing.Point(53, 150);
            this.tbTEAKey.Multiline = true;
            this.tbTEAKey.Name = "tbTEAKey";
            this.tbTEAKey.Size = new System.Drawing.Size(349, 37);
            this.tbTEAKey.TabIndex = 11;
            // 
            // tbTEAInput
            // 
            this.tbTEAInput.Location = new System.Drawing.Point(14, 60);
            this.tbTEAInput.Multiline = true;
            this.tbTEAInput.Name = "tbTEAInput";
            this.tbTEAInput.Size = new System.Drawing.Size(388, 84);
            this.tbTEAInput.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(136, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "TEA / PCBC";
            // 
            // pStart
            // 
            this.pStart.Controls.Add(this.label12);
            this.pStart.Location = new System.Drawing.Point(0, 27);
            this.pStart.Name = "pStart";
            this.pStart.Size = new System.Drawing.Size(449, 416);
            this.pStart.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(25, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(390, 25);
            this.label12.TabIndex = 0;
            this.label12.Text = "Choose Algorithm before starting FSW!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 445);
            this.Controls.Add(this.pStart);
            this.Controls.Add(this.pTEA);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pCodebook);
            this.Controls.Add(this.pRC6);
            this.Name = "Form1";
            this.Text = "Decypher";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodebook2)).EndInit();
            this.pCodebook.ResumeLayout(false);
            this.pCodebook.PerformLayout();
            this.pRC6.ResumeLayout(false);
            this.pRC6.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pTEA.ResumeLayout(false);
            this.pTEA.PerformLayout();
            this.pStart.ResumeLayout(false);
            this.pStart.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox tbInput2;
        private System.Windows.Forms.Button btnSaveCodebook;
        private System.Windows.Forms.DataGridView dgvCodebook2;
        private System.Windows.Forms.Button btnDekriptuj;
        private System.Windows.Forms.TextBox tbOutput2;
        private System.Windows.Forms.Panel pCodebook;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuAlgorithm;
        private System.Windows.Forms.ToolStripMenuItem codebookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rC6ToolStripMenuItem;
        private System.Windows.Forms.Panel pRC6;
        private System.Windows.Forms.TextBox tbRC6Input;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbRC6Key;
        private System.Windows.Forms.Button btnRC6;
        private System.Windows.Forms.TextBox tbRC6Output;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem tEAPCBCToolStripMenuItem;
        private System.Windows.Forms.Panel pTEA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnTEADecrypt;
        private System.Windows.Forms.TextBox tbTEAOutput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbTEAKey;
        private System.Windows.Forms.TextBox tbTEAInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbIV;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pStart;
        private System.Windows.Forms.Label label12;
    }
}

