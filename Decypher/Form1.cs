﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decypher
{
    public partial class Form1 : Form
    {
        private A1_Codebook codebook;
        private A2_RC6 rc6;
        private A3_TEA_PCBC tea;

        private const int portNum = 5454;
        delegate void SetTextCallback(string text);


        TcpClient client;
        NetworkStream ns;
        Thread t = null;
        private const string hostName = "localhost";

        public Form1()
        {
            InitializeComponent();
          
            codebook = new A1_Codebook();
            codebook.ReadFromFile();
            SetCodebook();

            rc6 = new A2_RC6();
            tea = new A3_TEA_PCBC();

            Random r = new Random();

            client = new TcpClient(hostName, portNum);
            ns = client.GetStream();
            int s = r.Next(0,999999);
            byte[] byteTime = Encoding.ASCII.GetBytes(s.ToString());
            ns.Write(byteTime, 0, byteTime.Length);
            t = new Thread(DoWork);
            t.Start();
        }


        public void DoWork()
        {
            byte[] bytes = new byte[1024];
            while (true)
            {
                int bytesRead = ns.Read(bytes, 0, bytes.Length);
                if (pCodebook.Visible == true)
                {
                    this.SetText(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                }
                else if (pRC6.Visible == true)
                {
                    this.SetText1(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                }
                else if (pTEA.Visible == true)
                {
                    this.SetText2(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                }
            }
        }
        private void SetText(string text)
        {
            codebook.ReadFromFile();

            FileStream fOpen = File.OpenRead(text);
            byte[] txt = new byte[fOpen.Length];

            fOpen.Read(txt, 0, txt.Length);
            var str = System.Text.Encoding.Default.GetString(txt);
            string code = codebook.Dekriptuj(str.ToString());
            
            string name = System.IO.Path.GetFileName(text);
            name = System.IO.Path.GetFileNameWithoutExtension(name);

            using (BinaryWriter writer = new BinaryWriter(File.Open(@"D:\decrypted\" + name + ".txt", FileMode.Create)))
            {
                writer.Write(code);
            }
            
        }

        private void SetText1(string text)
        {
            byte[] key = Encoding.ASCII.GetBytes("1234567");
            byte[] byteText;
            byteText = File.ReadAllBytes(text);
            string name = System.IO.Path.GetFileName(text);
            name = System.IO.Path.GetFileNameWithoutExtension(name);
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"D:\decrypted\" + name + ".txt", FileMode.Create)))
            {
                writer.Write(A2_RC6.decrypt(byteText, key));
            }
        }

        private void SetText2(string text)
        {
            byte[] key = Encoding.ASCII.GetBytes("1234567890123456");
            byte[] iv = Encoding.ASCII.GetBytes("123456789012");

            byte[] byteText;

            tea = new A3_TEA_PCBC(key, iv);
            byteText = File.ReadAllBytes(text);

            string name = System.IO.Path.GetFileName(text);
            name = System.IO.Path.GetFileNameWithoutExtension(name);
            using (BinaryWriter writer = new BinaryWriter(File.Open(@"D:\decrypted\" + name + ".txt", FileMode.Create)))
            {
                writer.Write(tea.decrypt(byteText));
            }
        }

        #region FSW

        private void SetCodebook()
        {
            var items = from pair in codebook.recnik
                        orderby pair.Key.Length descending
                        select pair;

            codebook.recnik = items.ToDictionary(x => x.Key, x => x.Value);

            var list = from el in codebook.recnik
                       select new
                       {
                           word = el.Key,
                           code = el.Value.ToString(),
                       };

            dgvCodebook2.DataSource = list.ToArray();
            dgvCodebook2.Refresh();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddInCodebook testDialog = new AddInCodebook();

            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {
                string newWord = testDialog.t1.Text;
                string newCode = testDialog.t2.Text;

                codebook.recnik.Add(newWord, Int32.Parse(newCode));

                SetCodebook();
            }

            testDialog.Dispose();
        }

        private void btnSaveCodebook_Click(object sender, EventArgs e)
        {
            codebook.SaveToFile();
            SetCodebook();
        }

        private void btnDekriptuj_Click(object sender, EventArgs e)
        {
            tbOutput2.Text = codebook.Dekriptuj(tbInput2.Text);
        }

        #endregion

        private void rC6ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pCodebook.Visible = false;
            pStart.Visible = false;
            pRC6.Visible = true;
            pTEA.Visible = false;
        }

        private void btnRC6_Click(object sender, EventArgs e)
        {
            byte[] key = Encoding.ASCII.GetBytes(tbRC6Key.Text);
            byte[] byteText = Encoding.ASCII.GetBytes(tbRC6Input.Text);

            tbRC6Output.Text = Encoding.ASCII.GetString(A2_RC6.decrypt(byteText, key));
            
        }

        private void tEAPCBCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pCodebook.Visible = false;
            pStart.Visible = false;
            pRC6.Visible = false;
            pTEA.Visible = true;
        }

        private void codebookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pCodebook.Visible = true;
            pRC6.Visible = false;
            pStart.Visible = false;
            pTEA.Visible = false;
        }

        private void btnTEADecrypt_Click(object sender, EventArgs e)
        {
            byte[] key = Encoding.ASCII.GetBytes(tbTEAKey.Text);
            byte[] byteText = Encoding.ASCII.GetBytes(tbTEAInput.Text);
            byte[] iv = Encoding.ASCII.GetBytes(tbIV.Text);

            tea = new A3_TEA_PCBC(key, iv);
            tbTEAOutput.Text = Encoding.ASCII.GetString(tea.decrypt(byteText));
        }
    }
}
