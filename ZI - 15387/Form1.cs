﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZI___15387
{
    public partial class Form1 : Form
    {
        private A1_Codebook codebook;
        private A2_RC6 rc6;
        private A3_TEA_PCBC tea;
        private A4_Knapsack knapsack;

        private const int portNum = 4545;
        delegate void SetTextCallback(string text);

        TcpClient client;
        NetworkStream ns;
        Thread t = null;
        private const string hostName = "localhost";
        
        public Form1()
        {
            InitializeComponent();
            
            codebook = new A1_Codebook();
            codebook.ReadFromFile();
            SetCodebook();

            rc6 = new A2_RC6();
            tea = new A3_TEA_PCBC();
            knapsack=new A4_Knapsack();

            Random r = new Random();

            client = new TcpClient(hostName, portNum);
            ns = client.GetStream();
            int s = r.Next(0, 999999);
            byte[] byteTime = Encoding.ASCII.GetBytes(s.ToString());
            ns.Write(byteTime, 0, byteTime.Length);
            t = new Thread(DoWork);
            t.Start();
        }

        public void DoWork()
        {
            byte[] bytes = new byte[1024];
            while (true)
            {
                int bytesRead = ns.Read(bytes, 0, bytes.Length);
                if (pCodebookCrypt.Visible == true)
                {
                    this.SetText(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                }
                else if (pRC6Cypher.Visible == true)
                {
                    this.SetText1(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                }
                else if (pTEA.Visible == true)
                {
                    this.SetText2(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                }
            }
        }
        private void SetText(string text)
        {
            codebook.ReadFromFile();
            
            FileStream fOpen = File.OpenRead(text);
            byte[] txt = new byte[fOpen.Length];

            fOpen.Read(txt, 0, txt.Length);
            var str = System.Text.Encoding.Default.GetString(txt);
            string code = codebook.Kriptuj(str.ToString());

            string name = System.IO.Path.GetFileName(text);
            name = System.IO.Path.GetFileNameWithoutExtension(name);

            using (BinaryWriter writer = new BinaryWriter(File.Open(@"D:\todecrypt\" + name + ".bin", FileMode.Create)))
            {
                writer.Write(code);
            }
        }

        private void SetText1(string text)
        {
            byte[] key = Encoding.ASCII.GetBytes("1234567");
            byte[] byteText;

            byteText = A2_RC6.encrypt(@File.ReadAllBytes(text), key);

            string name = System.IO.Path.GetFileName(text);
            name = System.IO.Path.GetFileNameWithoutExtension(name);

            using (BinaryWriter writer = new BinaryWriter(File.Open(@"D:\todecrypt\" + name + ".bin", FileMode.Create)))
            {
                writer.Write(byteText);
            }

        }

        private void SetText2(string text)
        {
            byte[] key = Encoding.ASCII.GetBytes("1234567890123456");
            byte[] iv = Encoding.ASCII.GetBytes("123456789012");

            byte[] byteText;

            tea = new A3_TEA_PCBC(key, iv);
            byteText = tea.crypt(@File.ReadAllBytes(text));

            string name = System.IO.Path.GetFileName(text);
            name = System.IO.Path.GetFileNameWithoutExtension(name);

            using (BinaryWriter writer = new BinaryWriter(File.Open(@"D:\todecrypt\" + name + ".bin", FileMode.Create)))
            {
                writer.Write(byteText);
            }
        }

        private void SetCodebook()
        {
            var items = from pair in codebook.recnik
                        orderby pair.Key.Length descending
                        select pair;

            codebook.recnik = items.ToDictionary(x => x.Key, x => x.Value);

            var list = from el in codebook.recnik
                       select new
                       {
                           word = el.Key,
                           code = el.Value.ToString(),
                       };

            dgvCodebookCrypt.DataSource = list.ToArray();
            dgvCodebookCrypt.Refresh();
            
        }

        private void btnKriptuj_Click(object sender, EventArgs e)
        {
            tbOutput.Text = codebook.Kriptuj(tbInput.Text);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddInCodebook testDialog = new AddInCodebook();

            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {
                string newWord = testDialog.t1.Text;
                string newCode = testDialog.t2.Text;

                codebook.recnik.Add(newWord, Int32.Parse(newCode));

                SetCodebook();
            }

            testDialog.Dispose();

        }

        private void btnSaveCB_Click(object sender, EventArgs e)
        {
            codebook.SaveToFile();
            SetCodebook();
        }

        private void codebookToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            pRC6Cypher.Visible = false;
            pKnapsack.Visible = false;
            pTEA.Visible = false;
            pStart.Visible = false;
            pCodebookCrypt.Visible = true;
        }

        private void rC6ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pCodebookCrypt.Visible = false;
            pKnapsack.Visible = false;
            pTEA.Visible = false;
            pStart.Visible = false;
            pRC6Cypher.Visible = true;
        }

        private void btnRC6encrypt_Click(object sender, EventArgs e)
        {
            byte[] byteText = Encoding.ASCII.GetBytes(tbRC6Input.Text);
            byte[] key = Encoding.ASCII.GetBytes(tbRC6Key.Text);

            tbRC6Output.Text = Encoding.ASCII.GetString(A2_RC6.encrypt(byteText, key));

        }

        private void tEAPCBCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pCodebookCrypt.Visible = false;
            pRC6Cypher.Visible = false;
            pKnapsack.Visible = false;
            pStart.Visible = false;
            pTEA.Visible = true;
        }

        private void btnTEAencrypt_Click(object sender, EventArgs e)
        {
            byte[] key = Encoding.ASCII.GetBytes(tbTEAKey.Text);
            byte[] byteText = Encoding.ASCII.GetBytes(tbTEAInput.Text);
            byte[] iv = Encoding.ASCII.GetBytes(tbTEAIV.Text);

            tea = new A3_TEA_PCBC(key, iv);
            tbTEAOutput.Text = Encoding.ASCII.GetString(tea.crypt(byteText));
        }

        private void tbTEAKey_Leave(object sender, EventArgs e)
        {
            if (tbTEAKey.Text.Length < 16)
                tbTEAKey.Focus();
        }

        private void btnPrivateKey_Click(object sender, EventArgs e)
        {
            string[] privateKey = tbPrivateKey.Text.Split(',');
            string publicKey = "";

            int m = System.Convert.ToInt32(tbM.Text);
            int n = System.Convert.ToInt32(tbN.Text);
            knapsack.N = n;
            knapsack.M = m;

            int k = 1;
            int rez = -1;
            int im = -1;

            do
            {
                im = (n * k + 1) / m;
                k++;
                rez = (im * m) % n;
            }
            while (rez != 1);

            knapsack.Im = im;
            lblIM.Text = im.ToString();

            int i = 0;
            int[] pkey=new int[8];
            int[] pubkey = new int[8];

            foreach (string pk in privateKey)
            {
                pkey[i] = Convert.ToInt32(pk);

                pubkey[i] = (pkey[i] * m) % n;

                if (i != 7)
                    publicKey += pubkey[i].ToString() + ",";
                else
                    publicKey += pubkey[i].ToString();

                i++;
            }

            knapsack.PublicKey = pubkey;
            knapsack.PrivateKey = pkey;

            tbPublicKey.Text = publicKey;
        }

        private void btnKnapCrypt_Click(object sender, EventArgs e)
        {
            tbKnapsackOutput.Text = knapsack.Kriptuj(tbKnapsackInput.Text);
        }

        private void btnKnapDecrypt_Click(object sender, EventArgs e)
        {
            tbKnapsackInput.Text = knapsack.Dekriptuj(tbKnapsackOutput.Text);
        }

        private void btnSimulation_Click(object sender, EventArgs e)
        {
            string txt = tbKnapsackInput.Text;
            String[] words = tbKnapsackInput.Text.Split(' ');

            foreach (String word in words)
            {
                tbKnapsackInput.Text = word;

                tbKnapsackOutput.Text = knapsack.Kriptuj(word);

                DateTime time = DateTime.Now;
                do
                {
                    Application.DoEvents();
                } while (time.AddSeconds(2) > DateTime.Now);

            }
            tbKnapsackInput.Text = txt;
            tbKnapsackOutput.Text = knapsack.Kriptuj(txt);
        }

        private void knapsackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pCodebookCrypt.Visible = false;
            pRC6Cypher.Visible = false;
            pTEA.Visible = false;
            pStart.Visible = false;
            pKnapsack.Visible = true;
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            if(tbPublicKey.Text.Equals(string.Empty))
            {
                MessageBox.Show("Generate Public key, first!");
            }
            else if(ofdChooseFile.ShowDialog()==DialogResult.OK)
            {
                string file = ofdChooseFile.FileName;
                string text = @File.ReadAllText(file);
                string ext = Path.GetExtension(file);
                string name = Path.GetFileNameWithoutExtension(file);

                lblChosenFile.Text = ofdChooseFile.FileName;
                string coded = knapsack.Kriptuj(text);
                File.WriteAllText("D:\\knapsacktodecrypt\\"+name+ext, coded);
                
                string readCoded = File.ReadAllText("D:\\knapsacktodecrypt\\" + name + ext);
                File.WriteAllText("D:\\knapsackdecrypted\\" + name + ext, knapsack.Dekriptuj(readCoded));
                lblCreatedFile.Text = "D:\\knapsackdecrypted\\" + name + ext;

            }
        }
    }
}
