﻿namespace ZI___15387
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbInput = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.btnKriptuj = new System.Windows.Forms.Button();
            this.dgvCodebookCrypt = new System.Windows.Forms.DataGridView();
            this.btnSaveCB = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuAlgorithm = new System.Windows.Forms.ToolStripMenuItem();
            this.codebookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rC6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEAPCBCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pCodebookCrypt = new System.Windows.Forms.Panel();
            this.pRC6Cypher = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tbRC6Key = new System.Windows.Forms.TextBox();
            this.btnRC6encrypt = new System.Windows.Forms.Button();
            this.tbRC6Output = new System.Windows.Forms.TextBox();
            this.tbRC6Input = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pTEA = new System.Windows.Forms.Panel();
            this.tbTEAOutput = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnTEAencrypt = new System.Windows.Forms.Button();
            this.tbTEAIV = new System.Windows.Forms.TextBox();
            this.tbTEAKey = new System.Windows.Forms.TextBox();
            this.tbTEAInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pKnapsack = new System.Windows.Forms.Panel();
            this.btnSimulation = new System.Windows.Forms.Button();
            this.btnKnapDecrypt = new System.Windows.Forms.Button();
            this.btnKnapCrypt = new System.Windows.Forms.Button();
            this.tbKnapsackOutput = new System.Windows.Forms.TextBox();
            this.tbKnapsackInput = new System.Windows.Forms.TextBox();
            this.lblIM = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnPrivateKey = new System.Windows.Forms.Button();
            this.tbN = new System.Windows.Forms.TextBox();
            this.tbM = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPublicKey = new System.Windows.Forms.TextBox();
            this.tbPrivateKey = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.knapsackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnChooseFile = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblChosenFile = new System.Windows.Forms.Label();
            this.lblCreatedFile = new System.Windows.Forms.Label();
            this.ofdChooseFile = new System.Windows.Forms.OpenFileDialog();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.pStart = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodebookCrypt)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.pCodebookCrypt.SuspendLayout();
            this.pRC6Cypher.SuspendLayout();
            this.pTEA.SuspendLayout();
            this.pKnapsack.SuspendLayout();
            this.pStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbInput
            // 
            this.tbInput.Location = new System.Drawing.Point(13, 49);
            this.tbInput.Multiline = true;
            this.tbInput.Name = "tbInput";
            this.tbInput.Size = new System.Drawing.Size(216, 100);
            this.tbInput.TabIndex = 0;
            // 
            // tbOutput
            // 
            this.tbOutput.Location = new System.Drawing.Point(13, 247);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(216, 100);
            this.tbOutput.TabIndex = 1;
            // 
            // btnKriptuj
            // 
            this.btnKriptuj.Location = new System.Drawing.Point(70, 168);
            this.btnKriptuj.Name = "btnKriptuj";
            this.btnKriptuj.Size = new System.Drawing.Size(108, 56);
            this.btnKriptuj.TabIndex = 2;
            this.btnKriptuj.Text = "encrypt";
            this.btnKriptuj.UseVisualStyleBackColor = true;
            this.btnKriptuj.Click += new System.EventHandler(this.btnKriptuj_Click);
            // 
            // dgvCodebookCrypt
            // 
            this.dgvCodebookCrypt.AllowUserToDeleteRows = false;
            this.dgvCodebookCrypt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCodebookCrypt.Location = new System.Drawing.Point(235, 7);
            this.dgvCodebookCrypt.Name = "dgvCodebookCrypt";
            this.dgvCodebookCrypt.Size = new System.Drawing.Size(185, 357);
            this.dgvCodebookCrypt.TabIndex = 4;
            // 
            // btnSaveCB
            // 
            this.btnSaveCB.Location = new System.Drawing.Point(235, 370);
            this.btnSaveCB.Name = "btnSaveCB";
            this.btnSaveCB.Size = new System.Drawing.Size(101, 25);
            this.btnSaveCB.TabIndex = 5;
            this.btnSaveCB.Text = "save codebook";
            this.btnSaveCB.UseVisualStyleBackColor = true;
            this.btnSaveCB.Click += new System.EventHandler(this.btnSaveCB_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(345, 370);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 25);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(53, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "CODEBOOK";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAlgorithm});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(452, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuAlgorithm
            // 
            this.menuAlgorithm.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codebookToolStripMenuItem,
            this.rC6ToolStripMenuItem,
            this.tEAPCBCToolStripMenuItem,
            this.knapsackToolStripMenuItem});
            this.menuAlgorithm.Name = "menuAlgorithm";
            this.menuAlgorithm.Size = new System.Drawing.Size(77, 20);
            this.menuAlgorithm.Text = "Algortithm";
            // 
            // codebookToolStripMenuItem
            // 
            this.codebookToolStripMenuItem.Name = "codebookToolStripMenuItem";
            this.codebookToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.codebookToolStripMenuItem.Text = "Codebook";
            this.codebookToolStripMenuItem.Click += new System.EventHandler(this.codebookToolStripMenuItem_Click);
            // 
            // rC6ToolStripMenuItem
            // 
            this.rC6ToolStripMenuItem.Name = "rC6ToolStripMenuItem";
            this.rC6ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.rC6ToolStripMenuItem.Text = "RC6";
            this.rC6ToolStripMenuItem.Click += new System.EventHandler(this.rC6ToolStripMenuItem_Click);
            // 
            // tEAPCBCToolStripMenuItem
            // 
            this.tEAPCBCToolStripMenuItem.Name = "tEAPCBCToolStripMenuItem";
            this.tEAPCBCToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tEAPCBCToolStripMenuItem.Text = "TEA / PCBC";
            this.tEAPCBCToolStripMenuItem.Click += new System.EventHandler(this.tEAPCBCToolStripMenuItem_Click);
            // 
            // pCodebookCrypt
            // 
            this.pCodebookCrypt.Controls.Add(this.label20);
            this.pCodebookCrypt.Controls.Add(this.label19);
            this.pCodebookCrypt.Controls.Add(this.label1);
            this.pCodebookCrypt.Controls.Add(this.btnAdd);
            this.pCodebookCrypt.Controls.Add(this.tbInput);
            this.pCodebookCrypt.Controls.Add(this.btnSaveCB);
            this.pCodebookCrypt.Controls.Add(this.btnKriptuj);
            this.pCodebookCrypt.Controls.Add(this.dgvCodebookCrypt);
            this.pCodebookCrypt.Controls.Add(this.tbOutput);
            this.pCodebookCrypt.Location = new System.Drawing.Point(9, 27);
            this.pCodebookCrypt.Name = "pCodebookCrypt";
            this.pCodebookCrypt.Size = new System.Drawing.Size(423, 401);
            this.pCodebookCrypt.TabIndex = 9;
            this.pCodebookCrypt.Visible = false;
            // 
            // pRC6Cypher
            // 
            this.pRC6Cypher.Controls.Add(this.label18);
            this.pRC6Cypher.Controls.Add(this.label17);
            this.pRC6Cypher.Controls.Add(this.label3);
            this.pRC6Cypher.Controls.Add(this.tbRC6Key);
            this.pRC6Cypher.Controls.Add(this.btnRC6encrypt);
            this.pRC6Cypher.Controls.Add(this.tbRC6Output);
            this.pRC6Cypher.Controls.Add(this.tbRC6Input);
            this.pRC6Cypher.Controls.Add(this.label2);
            this.pRC6Cypher.Location = new System.Drawing.Point(11, 29);
            this.pRC6Cypher.Name = "pRC6Cypher";
            this.pRC6Cypher.Size = new System.Drawing.Size(420, 397);
            this.pRC6Cypher.TabIndex = 10;
            this.pRC6Cypher.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Key";
            // 
            // tbRC6Key
            // 
            this.tbRC6Key.Location = new System.Drawing.Point(45, 137);
            this.tbRC6Key.Multiline = true;
            this.tbRC6Key.Name = "tbRC6Key";
            this.tbRC6Key.Size = new System.Drawing.Size(350, 41);
            this.tbRC6Key.TabIndex = 12;
            // 
            // btnRC6encrypt
            // 
            this.btnRC6encrypt.Location = new System.Drawing.Point(164, 193);
            this.btnRC6encrypt.Name = "btnRC6encrypt";
            this.btnRC6encrypt.Size = new System.Drawing.Size(75, 46);
            this.btnRC6encrypt.TabIndex = 11;
            this.btnRC6encrypt.Text = "encrypt";
            this.btnRC6encrypt.UseVisualStyleBackColor = true;
            this.btnRC6encrypt.Click += new System.EventHandler(this.btnRC6encrypt_Click);
            // 
            // tbRC6Output
            // 
            this.tbRC6Output.Location = new System.Drawing.Point(17, 254);
            this.tbRC6Output.Multiline = true;
            this.tbRC6Output.Name = "tbRC6Output";
            this.tbRC6Output.Size = new System.Drawing.Size(379, 130);
            this.tbRC6Output.TabIndex = 10;
            // 
            // tbRC6Input
            // 
            this.tbRC6Input.Location = new System.Drawing.Point(17, 45);
            this.tbRC6Input.Multiline = true;
            this.tbRC6Input.Name = "tbRC6Input";
            this.tbRC6Input.Size = new System.Drawing.Size(379, 85);
            this.tbRC6Input.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(175, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "RC6";
            // 
            // pTEA
            // 
            this.pTEA.Controls.Add(this.label16);
            this.pTEA.Controls.Add(this.label15);
            this.pTEA.Controls.Add(this.tbTEAOutput);
            this.pTEA.Controls.Add(this.label6);
            this.pTEA.Controls.Add(this.label5);
            this.pTEA.Controls.Add(this.btnTEAencrypt);
            this.pTEA.Controls.Add(this.tbTEAIV);
            this.pTEA.Controls.Add(this.tbTEAKey);
            this.pTEA.Controls.Add(this.tbTEAInput);
            this.pTEA.Controls.Add(this.label4);
            this.pTEA.Location = new System.Drawing.Point(11, 29);
            this.pTEA.Name = "pTEA";
            this.pTEA.Size = new System.Drawing.Size(417, 402);
            this.pTEA.TabIndex = 11;
            this.pTEA.Visible = false;
            // 
            // tbTEAOutput
            // 
            this.tbTEAOutput.Location = new System.Drawing.Point(10, 274);
            this.tbTEAOutput.Multiline = true;
            this.tbTEAOutput.Name = "tbTEAOutput";
            this.tbTEAOutput.Size = new System.Drawing.Size(391, 112);
            this.tbTEAOutput.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "IV";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Key";
            // 
            // btnTEAencrypt
            // 
            this.btnTEAencrypt.Location = new System.Drawing.Point(318, 185);
            this.btnTEAencrypt.Name = "btnTEAencrypt";
            this.btnTEAencrypt.Size = new System.Drawing.Size(75, 64);
            this.btnTEAencrypt.TabIndex = 13;
            this.btnTEAencrypt.Text = "encrypt";
            this.btnTEAencrypt.UseVisualStyleBackColor = true;
            this.btnTEAencrypt.Click += new System.EventHandler(this.btnTEAencrypt_Click);
            // 
            // tbTEAIV
            // 
            this.tbTEAIV.Location = new System.Drawing.Point(55, 185);
            this.tbTEAIV.Multiline = true;
            this.tbTEAIV.Name = "tbTEAIV";
            this.tbTEAIV.Size = new System.Drawing.Size(247, 64);
            this.tbTEAIV.TabIndex = 12;
            // 
            // tbTEAKey
            // 
            this.tbTEAKey.Location = new System.Drawing.Point(55, 142);
            this.tbTEAKey.MaxLength = 16;
            this.tbTEAKey.Multiline = true;
            this.tbTEAKey.Name = "tbTEAKey";
            this.tbTEAKey.Size = new System.Drawing.Size(346, 37);
            this.tbTEAKey.TabIndex = 11;
            this.tbTEAKey.Leave += new System.EventHandler(this.tbTEAKey_Leave);
            // 
            // tbTEAInput
            // 
            this.tbTEAInput.Location = new System.Drawing.Point(10, 51);
            this.tbTEAInput.Multiline = true;
            this.tbTEAInput.Name = "tbTEAInput";
            this.tbTEAInput.Size = new System.Drawing.Size(391, 85);
            this.tbTEAInput.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(135, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "TEA / PCBC";
            // 
            // pKnapsack
            // 
            this.pKnapsack.Controls.Add(this.lblCreatedFile);
            this.pKnapsack.Controls.Add(this.lblChosenFile);
            this.pKnapsack.Controls.Add(this.label14);
            this.pKnapsack.Controls.Add(this.label13);
            this.pKnapsack.Controls.Add(this.btnChooseFile);
            this.pKnapsack.Controls.Add(this.btnSimulation);
            this.pKnapsack.Controls.Add(this.btnKnapDecrypt);
            this.pKnapsack.Controls.Add(this.btnKnapCrypt);
            this.pKnapsack.Controls.Add(this.tbKnapsackOutput);
            this.pKnapsack.Controls.Add(this.tbKnapsackInput);
            this.pKnapsack.Controls.Add(this.lblIM);
            this.pKnapsack.Controls.Add(this.label12);
            this.pKnapsack.Controls.Add(this.btnPrivateKey);
            this.pKnapsack.Controls.Add(this.tbN);
            this.pKnapsack.Controls.Add(this.tbM);
            this.pKnapsack.Controls.Add(this.label11);
            this.pKnapsack.Controls.Add(this.label10);
            this.pKnapsack.Controls.Add(this.tbPublicKey);
            this.pKnapsack.Controls.Add(this.tbPrivateKey);
            this.pKnapsack.Controls.Add(this.label9);
            this.pKnapsack.Controls.Add(this.label8);
            this.pKnapsack.Controls.Add(this.label7);
            this.pKnapsack.Location = new System.Drawing.Point(10, 29);
            this.pKnapsack.Name = "pKnapsack";
            this.pKnapsack.Size = new System.Drawing.Size(432, 402);
            this.pKnapsack.TabIndex = 12;
            this.pKnapsack.Visible = false;
            // 
            // btnSimulation
            // 
            this.btnSimulation.Location = new System.Drawing.Point(329, 108);
            this.btnSimulation.Name = "btnSimulation";
            this.btnSimulation.Size = new System.Drawing.Size(93, 38);
            this.btnSimulation.TabIndex = 26;
            this.btnSimulation.Text = "Simulate";
            this.btnSimulation.UseVisualStyleBackColor = true;
            this.btnSimulation.Click += new System.EventHandler(this.btnSimulation_Click);
            // 
            // btnKnapDecrypt
            // 
            this.btnKnapDecrypt.Location = new System.Drawing.Point(190, 191);
            this.btnKnapDecrypt.Name = "btnKnapDecrypt";
            this.btnKnapDecrypt.Size = new System.Drawing.Size(54, 39);
            this.btnKnapDecrypt.TabIndex = 25;
            this.btnKnapDecrypt.Text = "decrypt";
            this.btnKnapDecrypt.UseVisualStyleBackColor = true;
            this.btnKnapDecrypt.Click += new System.EventHandler(this.btnKnapDecrypt_Click);
            // 
            // btnKnapCrypt
            // 
            this.btnKnapCrypt.Location = new System.Drawing.Point(190, 152);
            this.btnKnapCrypt.Name = "btnKnapCrypt";
            this.btnKnapCrypt.Size = new System.Drawing.Size(54, 39);
            this.btnKnapCrypt.TabIndex = 24;
            this.btnKnapCrypt.Text = "encrypt";
            this.btnKnapCrypt.UseVisualStyleBackColor = true;
            this.btnKnapCrypt.Click += new System.EventHandler(this.btnKnapCrypt_Click);
            // 
            // tbKnapsackOutput
            // 
            this.tbKnapsackOutput.Location = new System.Drawing.Point(250, 152);
            this.tbKnapsackOutput.Multiline = true;
            this.tbKnapsackOutput.Name = "tbKnapsackOutput";
            this.tbKnapsackOutput.Size = new System.Drawing.Size(179, 78);
            this.tbKnapsackOutput.TabIndex = 23;
            // 
            // tbKnapsackInput
            // 
            this.tbKnapsackInput.Location = new System.Drawing.Point(5, 152);
            this.tbKnapsackInput.Multiline = true;
            this.tbKnapsackInput.Name = "tbKnapsackInput";
            this.tbKnapsackInput.Size = new System.Drawing.Size(179, 78);
            this.tbKnapsackInput.TabIndex = 22;
            // 
            // lblIM
            // 
            this.lblIM.AutoSize = true;
            this.lblIM.Location = new System.Drawing.Point(306, 118);
            this.lblIM.Name = "lblIM";
            this.lblIM.Size = new System.Drawing.Size(17, 13);
            this.lblIM.TabIndex = 21;
            this.lblIM.Text = "im";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(284, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "im:";
            // 
            // btnPrivateKey
            // 
            this.btnPrivateKey.Location = new System.Drawing.Point(80, 113);
            this.btnPrivateKey.Name = "btnPrivateKey";
            this.btnPrivateKey.Size = new System.Drawing.Size(189, 23);
            this.btnPrivateKey.TabIndex = 19;
            this.btnPrivateKey.Text = "Get Private Key";
            this.btnPrivateKey.UseVisualStyleBackColor = true;
            this.btnPrivateKey.Click += new System.EventHandler(this.btnPrivateKey_Click);
            // 
            // tbN
            // 
            this.tbN.Location = new System.Drawing.Point(306, 82);
            this.tbN.Name = "tbN";
            this.tbN.Size = new System.Drawing.Size(42, 20);
            this.tbN.TabIndex = 18;
            this.tbN.Text = "491";
            // 
            // tbM
            // 
            this.tbM.Location = new System.Drawing.Point(306, 60);
            this.tbM.Name = "tbM";
            this.tbM.Size = new System.Drawing.Size(42, 20);
            this.tbM.TabIndex = 17;
            this.tbM.Text = "41";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(284, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "n:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(282, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "m:";
            // 
            // tbPublicKey
            // 
            this.tbPublicKey.Location = new System.Drawing.Point(80, 82);
            this.tbPublicKey.Name = "tbPublicKey";
            this.tbPublicKey.Size = new System.Drawing.Size(189, 20);
            this.tbPublicKey.TabIndex = 14;
            // 
            // tbPrivateKey
            // 
            this.tbPrivateKey.Location = new System.Drawing.Point(80, 60);
            this.tbPrivateKey.Name = "tbPrivateKey";
            this.tbPrivateKey.Size = new System.Drawing.Size(189, 20);
            this.tbPrivateKey.TabIndex = 13;
            this.tbPrivateKey.Text = "2, 3, 7, 14, 30, 57, 120, 251";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Private Key:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Public Key:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(137, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 25);
            this.label7.TabIndex = 10;
            this.label7.Text = "KNAPSACK";
            // 
            // knapsackToolStripMenuItem
            // 
            this.knapsackToolStripMenuItem.Name = "knapsackToolStripMenuItem";
            this.knapsackToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.knapsackToolStripMenuItem.Text = "Knapsack";
            this.knapsackToolStripMenuItem.Click += new System.EventHandler(this.knapsackToolStripMenuItem_Click);
            // 
            // btnChooseFile
            // 
            this.btnChooseFile.Location = new System.Drawing.Point(5, 271);
            this.btnChooseFile.Name = "btnChooseFile";
            this.btnChooseFile.Size = new System.Drawing.Size(179, 23);
            this.btnChooseFile.TabIndex = 27;
            this.btnChooseFile.Text = "Choose file";
            this.btnChooseFile.UseVisualStyleBackColor = true;
            this.btnChooseFile.Click += new System.EventHandler(this.btnChooseFile_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(187, 260);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Chosen file:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(187, 297);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Created file:";
            // 
            // lblChosenFile
            // 
            this.lblChosenFile.AutoSize = true;
            this.lblChosenFile.Location = new System.Drawing.Point(255, 260);
            this.lblChosenFile.Name = "lblChosenFile";
            this.lblChosenFile.Size = new System.Drawing.Size(10, 13);
            this.lblChosenFile.TabIndex = 30;
            this.lblChosenFile.Text = " ";
            // 
            // lblCreatedFile
            // 
            this.lblCreatedFile.AutoSize = true;
            this.lblCreatedFile.Location = new System.Drawing.Point(255, 297);
            this.lblCreatedFile.Name = "lblCreatedFile";
            this.lblCreatedFile.Size = new System.Drawing.Size(10, 13);
            this.lblCreatedFile.TabIndex = 31;
            this.lblCreatedFile.Text = " ";
            // 
            // ofdChooseFile
            // 
            this.ofdChooseFile.FileName = "openFileDialog1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 35);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Input text:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 257);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Output text:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Input text:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 238);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 15;
            this.label18.Text = "Output text:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 33);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Input text:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 231);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Output text:";
            // 
            // pStart
            // 
            this.pStart.Controls.Add(this.label22);
            this.pStart.Controls.Add(this.label21);
            this.pStart.Location = new System.Drawing.Point(2, 29);
            this.pStart.Name = "pStart";
            this.pStart.Size = new System.Drawing.Size(449, 416);
            this.pStart.TabIndex = 15;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(25, 175);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(390, 25);
            this.label21.TabIndex = 0;
            this.label21.Text = "Choose Algorithm before starting FSW!";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(97, 211);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(259, 20);
            this.label22.TabIndex = 1;
            this.label22.Text = "That doesn\'t include Knapsack!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 437);
            this.Controls.Add(this.pStart);
            this.Controls.Add(this.pKnapsack);
            this.Controls.Add(this.pTEA);
            this.Controls.Add(this.pRC6Cypher);
            this.Controls.Add(this.pCodebookCrypt);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Cypher";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodebookCrypt)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pCodebookCrypt.ResumeLayout(false);
            this.pCodebookCrypt.PerformLayout();
            this.pRC6Cypher.ResumeLayout(false);
            this.pRC6Cypher.PerformLayout();
            this.pTEA.ResumeLayout(false);
            this.pTEA.PerformLayout();
            this.pKnapsack.ResumeLayout(false);
            this.pKnapsack.PerformLayout();
            this.pStart.ResumeLayout(false);
            this.pStart.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbInput;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.Button btnKriptuj;
        private System.Windows.Forms.DataGridView dgvCodebookCrypt;
        private System.Windows.Forms.Button btnSaveCB;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuAlgorithm;
        private System.Windows.Forms.ToolStripMenuItem codebookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rC6ToolStripMenuItem;
        private System.Windows.Forms.Panel pCodebookCrypt;
        private System.Windows.Forms.Panel pRC6Cypher;
        private System.Windows.Forms.Button btnRC6encrypt;
        private System.Windows.Forms.TextBox tbRC6Output;
        private System.Windows.Forms.TextBox tbRC6Input;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbRC6Key;
        private System.Windows.Forms.ToolStripMenuItem tEAPCBCToolStripMenuItem;
        private System.Windows.Forms.Panel pTEA;
        private System.Windows.Forms.TextBox tbTEAOutput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnTEAencrypt;
        private System.Windows.Forms.TextBox tbTEAIV;
        private System.Windows.Forms.TextBox tbTEAKey;
        private System.Windows.Forms.TextBox tbTEAInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pKnapsack;
        private System.Windows.Forms.Button btnPrivateKey;
        private System.Windows.Forms.TextBox tbN;
        private System.Windows.Forms.TextBox tbM;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPublicKey;
        private System.Windows.Forms.TextBox tbPrivateKey;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblIM;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnKnapDecrypt;
        private System.Windows.Forms.Button btnKnapCrypt;
        private System.Windows.Forms.TextBox tbKnapsackOutput;
        private System.Windows.Forms.TextBox tbKnapsackInput;
        private System.Windows.Forms.Button btnSimulation;
        private System.Windows.Forms.ToolStripMenuItem knapsackToolStripMenuItem;
        private System.Windows.Forms.Button btnChooseFile;
        private System.Windows.Forms.Label lblCreatedFile;
        private System.Windows.Forms.Label lblChosenFile;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.OpenFileDialog ofdChooseFile;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pStart;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
    }
}

