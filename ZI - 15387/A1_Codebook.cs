﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZI___15387
{
    public class A1_Codebook
    {
        public Dictionary<string, int> recnik;
        public bool crypt;
        
        public A1_Codebook()
        {
            recnik = new Dictionary<string, int>();
        }

        private void SortRecnik()
        {
            var items = from pair in recnik
                        orderby pair.Key.Length descending
                        select pair;

            recnik = items.ToDictionary(x => x.Key, x => x.Value);
        }

        public void SaveToFile()
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            var fi = new System.IO.FileInfo(@"D:\Fakultet\4. godina\7. semestar\Zastita informacija\Projekat\ZI - 15387\codebook.bin");

            using (var binaryFile = fi.Create())
            {
                binaryFormatter.Serialize(binaryFile, recnik);
                binaryFile.Flush();
            }
        }

        public void ReadFromFile()
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            var fi = new System.IO.FileInfo(@"D:\Fakultet\4. godina\7. semestar\Zastita informacija\Projekat\ZI - 15387\codebook.bin");

            using (var binaryFile = fi.OpenRead())
            {
                recnik = (Dictionary<string, int>)binaryFormatter.Deserialize(binaryFile);
            }

            SortRecnik();

        }

        public String Kriptuj(string recenica)
        {
            recenica = recenica.ToLower();
            recenica = recenica.Replace(",","");
            recenica = recenica.Replace(".", "");
            recenica = recenica.Replace("!", "");
            recenica = recenica.Replace("?", "");

            string[] rec = recenica.Split(' ');
            List<string> keys1 = this.recnik.Keys.ToList();
            List<int> values1 = this.recnik.Values.ToList();

            for (int i = 0; i < rec.Length; i++)
            {
                if (keys1.Contains(rec[i]))
                {
                    int j = keys1.IndexOf(rec[i]);
                    rec[i] = values1[j].ToString();
                }
            }

            recenica = String.Join<string>(" ", rec.ToArray());        

            //List<string> keys = new List<string>(this.recnik.Keys);
            //List<int> values = new List<int>(this.recnik.Values);

            //var list = keys.Zip(values, (k, v) => new { Key = k, Value = v });

            //foreach (var l in list)
            //{
            //    recenica = recenica.Replace(l.Key, l.Value.ToString());
            //}

            return recenica;
        }

    }
}
