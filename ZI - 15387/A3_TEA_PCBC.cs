﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZI___15387
{
    public class A3_TEA_PCBC
    {
        private static int[] key;
        private int[] IV, IVpom;
        private static int delta;

        public A3_TEA_PCBC()
        {

        }

        public A3_TEA_PCBC(byte[] k, byte[] iv)
        {
            key = new int[4];
            key = new int[k.Length / 4];
            for (int i = 0; i < k.Length; i += 4)
                key[i / 4] = BitConverter.ToInt32(k, i);


            IV = new int[2];
            IVpom = new int[2];
            IV = new int[iv.Length / 4];
            for (int i = 0; i < iv.Length; i += 4)
                IV[i / 4] = BitConverter.ToInt32(iv, i);


            IVpom[0] = IV[0];
            IVpom[1] = IV[1];

            delta = unchecked((int)0x9e3779b9);
        }

        public byte[] cryptBlock(byte[] input)
        {

            int[] plain = new int[2];
            plain = new int[input.Length / 4];
            for (int i = 0; i < input.Length; i += 4)
                plain[i / 4] = BitConverter.ToInt32(input, i);


            int v0 = plain[0] ^ IVpom[0];
            int v1 = plain[1] ^ IVpom[1];

            int sum = 0;

            for (int i = 0; i < 32; i++)
            {
                sum += delta;
                v0 += ((v1 << 4) + key[0]) ^ (v1 + sum) ^ ((v1 >> 5) + key[1]);
                v1 += ((v0 << 4) + key[2]) ^ (v0 + sum) ^ ((v0 >> 5) + key[3]);
            }

            IVpom[0] = plain[0] ^ v0;
            IVpom[1] = plain[1] ^ v1;

            plain[0] = v0;
            plain[1] = v1;

            byte[] data = new byte[plain.Length * 4];

            for (int i = 0; i < plain.Length; i++)
                Array.Copy(BitConverter.GetBytes(plain[i]), 0, data, i * 4, 4);

            return data;
        }

        public byte[] crypt(byte[] data)
        {
            IVpom[0] = IV[0];
            IVpom[1] = IV[1];

            byte[] block = new byte[8];

            int length = 8 - data.Length % 8;
            byte[] padding = new byte[length];
            padding[0] = (byte)0x80;
            int i;
            for (i = 1; i < length; i++)
                padding[i] = 0;
            int count = 0;
            byte[] tmp = new byte[data.Length + length];

            for (i = 0; i < data.Length + length; i++)
            {
                if (i > 0 && i % 8 == 0)
                {
                    block = cryptBlock(block);
                    Array.Copy(block, 0, tmp, i - 8, block.Length);
                }

                if (i < data.Length)
                    block[i % 8] = data[i];
                else
                {
                    block[i % 8] = padding[count];
                    count++;
                    if (count > length - 1) count = 1;
                }
            }
            block = cryptBlock(block);
            Array.Copy(block, 0, tmp, i - 8, block.Length);
            return tmp;
        }

    }
}
