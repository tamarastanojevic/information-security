﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MainApp
{
    public class Klijent
    {
        int name;
        NetworkStream networkStream;
        string type;

        public Klijent(int n, NetworkStream ns, string t)
        {
            name = n;
            networkStream = ns;
            type = t;
        }

        public int Name { get => name; set => name = value; }
        public NetworkStream NetworkStream { get => networkStream; set => networkStream = value; }
        public string Type { get => type; set => type = value; }
    }
}
