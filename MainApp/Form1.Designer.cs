﻿namespace MainApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartFSW = new System.Windows.Forms.Button();
            this.btnStopFSW = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStartFSW
            // 
            this.btnStartFSW.Location = new System.Drawing.Point(30, 48);
            this.btnStartFSW.Name = "btnStartFSW";
            this.btnStartFSW.Size = new System.Drawing.Size(75, 23);
            this.btnStartFSW.TabIndex = 0;
            this.btnStartFSW.Text = "Start";
            this.btnStartFSW.UseVisualStyleBackColor = true;
            this.btnStartFSW.Click += new System.EventHandler(this.btnStartFSW_Click);
            // 
            // btnStopFSW
            // 
            this.btnStopFSW.Location = new System.Drawing.Point(121, 48);
            this.btnStopFSW.Name = "btnStopFSW";
            this.btnStopFSW.Size = new System.Drawing.Size(75, 23);
            this.btnStopFSW.TabIndex = 1;
            this.btnStopFSW.Text = "Stop";
            this.btnStopFSW.UseVisualStyleBackColor = true;
            this.btnStopFSW.Click += new System.EventHandler(this.btnStopFSW_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(83, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "FSW";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(226, 88);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStopFSW);
            this.Controls.Add(this.btnStartFSW);
            this.Name = "Form1";
            this.Text = "Main APP 15387";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartFSW;
        private System.Windows.Forms.Button btnStopFSW;
        private System.Windows.Forms.Label label1;
    }
}

